CREATE DATABASE vsante1
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'French_France.utf8'
    LC_CTYPE = 'French_France.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

	create table article (
	   id_article           	 	SERIAL               not null,
	   title 						TEXT				 not null,
	   unpublished_date 			TIMESTAMP 			 DEFAULT null,
	   published 	 				TIMESTAMP            null,
	   summary 						TEXT 				 null,
	   summary_internal	 			TEXT				 null,
	   link_site	  				TEXT           		 not null,
	   date_creation 				TIMESTAMP 			 DEFAULT CURRENT_TIMESTAMP,
	   primary key (id_article),
	   UNIQUE (title, summary)
	);


	create table flux (
	   id_flux          	SERIAL               not null,
	   name_flux           	TEXT           		 not null UNIQUE,
	   link_flux            CHAR(100)            not null UNIQUE,
	   last_modified        TIMESTAMP            null,
	   language 			CHAR(32)			 not null,

	   primary key (id_flux)
	);


	create table block (
	   id_block          	SERIAL               not null,
	   id_flux 				INTEGER				 not null,
	   id_article 			INTEGER				 not null,
	   date_creation 		TIMESTAMP 			 DEFAULT CURRENT_TIMESTAMP,

	   primary key (id_block),
	   foreign key (id_flux) references flux(id_flux),
	   foreign key (id_article) references article(id_article),
	   UNIQUE (id_flux, id_article)
	);