# INSERT QUERIES
insert_article = "INSERT INTO article (title, published, summary, summary_internal, link_site) VALUES (BTRIM(%s, ' '), %s, %s, %s, BTRIM(%s, ' '));"

insert_flux    = "INSERT INTO flux (name_flux, link_flux, last_modified, language) VALUES (BTRIM(%s, ' '), BTRIM(%s, ' '), %s, %s);"

insert_block = "INSERT INTO block (id_flux, id_article) VALUES (%s, %s);"

# SELECT QUERIES
select_id_flux = "SELECT id_flux FROM flux WHERE name_flux = %s;"

select_id_article = "SELECT id_article FROM article WHERE title = %s;"

select_all_flux = "SELECT json_agg(flux) FROM flux;"

select_last_published_articles = "select json_agg(article) from article inner join block on article.id_article = block.id_article inner join flux on flux.id_flux = block.id_flux where flux.name_flux = %s GROUP BY article.published  ORDER BY article.published DESC LIMIT %s;"

select_last_articles = "select json_agg(article) from article inner join block on article.id_article = block.id_article inner join flux on flux.id_flux = block.id_flux GROUP BY article.published  ORDER BY article.published DESC LIMIT 500;"

