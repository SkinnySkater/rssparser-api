import random
import os, json
import hashlib
import string
from flask import jsonify
from flask_cors import CORS , cross_origin
from flask import Flask, request
from flask_mail import Mail, Message
from flask_restful import Resource, Api, abort
from werkzeug.security import generate_password_hash, check_password_hash
import logging
import configparser
from dateutil.parser import parse
from query import *
from database import DB


CONFIG_PATH = "config.ini"
configfile_name = os.path.join(os.path.dirname(__file__), CONFIG_PATH)
config = configparser.ConfigParser()
config.read(configfile_name)

app = Flask(__name__)
cors = CORS(app, resources={r"/" + config['SERVER_FLASK']['API_VERSION'] + "/*": {"origins": "*"}})
api = Api(app)
mail = Mail(app)

file_handler = logging.FileHandler(config['DEFAULT']['LOG_PATH'])
app.logger.addHandler(file_handler)
app.logger.setLevel(logging.INFO)



"""
    HEADER STYLE:
{
  "flux_rss":{
    "name": "santé publique",
    "link": "https://www.anses.fr/fr/theme-alimentation.rss",
    "last_modified": "Sat, 26 Jul 1997 05:00:00 GMT",
    "language": "fr-FR"
  },
  "articles": [
    {
      "title": "https://ansm.sante.fr/syndication/rss",
      "summary": "BIG TEXT",
      "summary_internal": "BIGGER TEXT",
      "link_site": "https://ansm.sante.fr/syndication/rss"
      "published": "Sat, 26 Jul 1997 05:00:00 GMT"
    }
  ]
}
"""


class InsertFlux(Resource):

    def post(self):
        content = request.get_json(silent=False)
        app.logger.info(content)
        flux_d = content['flux_rss']
        app.logger.debug(flux_d)
        f_params = [flux_d['name'], flux_d['link'], parse(flux_d['last_modified']), flux_d['language']]
        db = DB(app)
        # Insert flux Table:
        db.insert_query(insert_flux, f_params)
        # Fet Id flux
        idf = int(db.select_query(select_id_flux, [flux_d['name'], ])[0][0])
        logging.debug(idf)
        for data in content['articles']:
            logging.debug(data)
            a_params = [data['title'].replace('"', '\"').replace("'", "\'"), parse(data['published']), data['summary'].replace('"', '\"').replace("'", "\'"), data['summary_internal'].replace('"', '\"').replace("'", "\'"), data['link_site']]
            db.insert_query(insert_article, a_params)
            # Get id article just inserted
            tmp = db.select_query(select_id_article, [data['title'].replace('"', '\"').replace("'", "\'"), ])
            app.logger.debug(data['title'])
            app.logger.debug(tmp)
            ida = int(tmp[0][0])
            app.logger.debug(ida)
            # Insert in Block now
            db.insert_query(insert_block, [idf, ida])
        req_resp = {}
        req_resp['code'] = 200
        req_resp['message'] = "All data has been successfully inserted."
        res = req_resp
        return res, 200


class GetRSSFlux(Resource):
    def get(self, action):
        if action == 'all':
            db = DB(app)
            req_resp = {}
            req_resp['code'] = 200
            req_resp['message'] = "All Flux as array."
            req_resp['data'] = db.select_query(select_all_flux)[0]
            res = req_resp
            return res, 200

def parsedate(date):
    return date.replace('T', ' ')[:16]

class GetRSSArticle(Resource):
    def get(self, action):
        if action == 'last':
            args = request.args
            print(args)
            db = DB(app)
            params = [args['flux_name'], args['limit']]
            print(params)
            tmp = db.select_query(select_last_published_articles, params)
            data = [t[0][0] for t in tmp]
            req_resp = {}
            req_resp['code'] = 200
            req_resp['iTotalRecords'] = len(data)
            req_resp['iTotalDisplayRecords'] = int(config['DATA']['iTotalDisplayRecords'])
            req_resp['sEcho'] = 0
            req_resp['sColumns'] = ''
            req_resp['message'] = "Retrieve all last published"
            req_resp['aaData'] = data
            res = req_resp
            return res, 200

    def post(self, action):
        if action == 'last':
            args = request.args
            db = DB(app)
            tmp = db.select_query(select_last_articles)
            data = [t[0][0] for t in tmp]
            for d in data:
                d['published'] = parsedate(d['published'])
            req_resp = {}
            # req_resp['iTotalRecords'] = len(data)
            # req_resp['iTotalDisplayRecords'] = 50
            # req_resp['sEcho'] = 0
            # req_resp['sColumns'] = ''
            req_resp['data'] = data
            res = req_resp
            return res, 200


"""
{
"flux_rss":{
    "name": "santé publique",
    "link": "https://www.anses.fr/fr/theme-alimentation.rss",
    "last_modified": "Sat, 26 Jul 1997 05:00:00 GMT",
    "language": "fr-FR"
  }
}
"""


class AddNewFlux(Resource):
    def post(self):
        content = request.get_json(silent=False)
        flux_d = content['flux_rss']
        app.logger.debug(flux_d)
        f_params = [flux_d['name'], flux_d['link'], parse(flux_d['last_modified']), flux_d['language']]
        db = DB(app)
        # Insert flux Table:
        res = db.insert_query(insert_flux, f_params)
        req_resp = {}
        if res:
            req_resp['code'] = 200
            req_resp['message'] = "Flux successfully added."
            res = req_resp
            return res, 200
        else:
            req_resp['code'] = 403
            req_resp['message'] = "Insertion of the flux failed, change Name or url."
            res = req_resp
            return res, 403

##
## Actually setup the Api resource routing here
##
api.add_resource(InsertFlux, '/{}/add_flux'.format(config['SERVER_FLASK']['API_VERSION']))
api.add_resource(GetRSSFlux, '/{}/flux/<string:action>'.format(config['SERVER_FLASK']['API_VERSION']))
api.add_resource(AddNewFlux, '/{}/flux/add_new'.format(config['SERVER_FLASK']['API_VERSION']))
api.add_resource(GetRSSArticle, '/{}/article/<string:action>'.format(config['SERVER_FLASK']['API_VERSION']))

if __name__ == '__main__':
    app.run(host=config['SERVER_FLASK']['FLASK_SERVER_HOST'], port=config['SERVER_FLASK']['PORT_SERVER'], debug=True)

