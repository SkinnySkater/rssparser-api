import psycopg2
import os
import configparser

CONFIG_PATH = "config.ini"


class DB(object):

    def __init__(self, app):
        self.app = app
        configfile_name = os.path.join(os.path.dirname(__file__), CONFIG_PATH)
        config = configparser.ConfigParser()
        config.read(configfile_name)
        self._db_connection = psycopg2.connect(dbname=config['DEFAULT']['PGSQL_DATABASE']
                                               , user=config['DEFAULT']['PGSQL_USER']
                                               , host=config['DEFAULT']['PGSQL_HOST']
                                               , password=config['DEFAULT']['PGSQL_PASSWORD']
                                               , port=config['DEFAULT']['PGSQL_PORT'])
        self._db_cur = self._db_connection.cursor()

    def insert_query(self, query, params):
        try:
            print(params)
            cur = self._db_cur
            cur.execute(query, params)
            #cur.close()
            self._db_connection.commit()
        except Exception as e:
            self.app.logger.error(e)
            self._db_connection.rollback()
            return False
        return True

    def select_query(self, query, params=None):
        try:
            cur = self._db_cur
            if params is None:
                cur.execute(query)
            else:
                cur.execute(query, params)
            data_json = cur.fetchall()
            #cur.close()
            self._db_connection.commit()
        except Exception as e:
            self.app.logger.error(e)
            return None
        return data_json

    def __del__(self):
        self._db_connection.close()

