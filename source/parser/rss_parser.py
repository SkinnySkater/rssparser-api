import feedparser
import configparser
import logging
import os, time, json, re
import sys
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests

CONFIG_PATH = "config.ini"
configfile_name = os.path.join(os.path.dirname(__file__), CONFIG_PATH)

config = None

NF = "N/A"


def init(configFile=configfile_name):
    """
            Init the Init file to let the parser know the which data to process on.

            Parameters
            ----------
            configFile : string
                name of config file .

            Returns
            -------
            config
                Object config which hold data as dictionary

    """
    config = configparser.ConfigParser()
    config.read(configFile)
    return config


def get_domain(url):
    parsed_url = urlparse(url)
    domain = str(parsed_url.netloc)
    return domain


# [ EXP for x in seq if COND ]
# clean the gathered links that may contain whitespaces and special white characters.
def clean_url(link_array):
    cleaned_links = [" ".join(s.split()) for s in link_array if len(s) > 0]
    return cleaned_links


def clean_data(text, tag=False):
    clean = text
    if tag:
        c = BeautifulSoup(text, "html.parser").text.strip()
        clean = c if c != '' or c is not None else clean
    c = clean.replace('\n', '').replace('\t', '')
    clean = c if c != '' or c is not None else clean
    c = " ".join(clean.split())
    clean = c if c != '' or c is not None else clean
    return clean


def isBlockChangedEtag(feed, url):
    # store the etag and modified
    last_etag = feed.etag
    last_modified = feed.modified

    # check if new version exists
    feed_update = feedparser.parse(url, etag=last_etag, modified=last_modified)

    if feed_update.status == 304:
        return False
    return True

"""
    HEADER STYLE:
{
  "flux_rss":{
    "name": "santé publique",
    "link": "https://www.anses.fr/fr/theme-alimentation.rss",
    "last_modified": "Sat, 26 Jul 1997 05:00:00 GMT",
    "language": "fr-FR"
  },
  "articles": [
    {
      "title": "https://ansm.sante.fr/syndication/rss",
      "summary": "BIG TEXT",
      "summary_internal": "BIGGER TEXT",
      "link_site": "https://ansm.sante.fr/syndication/rss"
      "published": "Sat, 26 Jul 1997 05:00:00 GMT"
    }
  ]
}
"""
def build_json(header, entries, name, link):
    articles = []
    h = json.loads(json.dumps(header))
    print(h.keys())
    data = {}
    data['flux_rss'] = {}
    data['flux_rss']['name'] = name
    data['flux_rss']['link'] = link
    if "content-language" in str(h.keys()).lower():
        if "Content-language" in h.keys():
            data['flux_rss']['language'] = h["Content-language"]
        elif "Content-Language" in h.keys():
            data['flux_rss']['language'] = h["Content-Language"]
        else:
            data['flux_rss']['language'] = NF
    else:
        data['flux_rss']['language'] = NF
    if 'Last-Modified' in h:
        data['flux_rss']['last_modified'] = h["Last-Modified"]
    elif 'date' in str(h).lower():
        if 'Date' in h:
            data['flux_rss']['last_modified'] = h['Date']
        elif 'date' in h:
            data['flux_rss']['last_modified'] = h['date']
        else:
            data['flux_rss']['last_modified'] = NF
    else:
        data['flux_rss']['last_modified'] = NF

    for d in entries:
        article = {}
        article['title'] = fill_or_set_default('title', d)
        article['link_site'] = fill_or_set_default('link', d)
        article['summary'] = fill_or_set_default('summary', d, True)
        if 'summary_detail' in d and 'value' in d['summary_detail']:
            article['summary_internal'] = clean_data(d['summary_detail']['value'])
        else:
            article['summary_internal'] = NF
        article['published'] = fill_or_set_default('published', d)
        articles.append(article)
    data['articles'] = {}
    data['articles'] = articles
    return data


def fill_or_set_default(key, data, clean=False):
    if key in data:
        if clean:
            return clean_data(data[key], True)
        else:
            return data[key]
    else:
        return NF


def send_data_api(data):
    print(config['DEFAULT']['URL_API'])
    try:
        r = requests.post(config['DEFAULT']['URL_API'], headers={'Content-Type': 'application/json'}, json=data)
        print(r.content)
        if r.status_code == 200:
            return True
    except Exception as error:
        print(error)
    return False


def run_app(link_array):
    i = 0
    for link in link_array:
        rss = feedparser.parse(link)
        get_domain(link)
        name = get_domain(link)+str(i)
        jdata = json.dumps(build_json(rss.headers, rss.entries, name, link))
        print(rss.headers)
        send_data_api(json.loads(jdata))
        i += 1


def write_to_file(data, name):
    f = open(name, 'w', encoding='UTF-8')  # The file is newly created where foo.py is
    for l in data:
        f.write(str(l))
    f.close()


if __name__ == '__main__':
    params = sys.argv[1:]
    if len(params) > 0:
        config = init(params[0])
    else:
        config = init()
    NF = config['DEFAULT']['NOT_FOUND']
    links = config['LINKS']['RSS_LINK'].split(config['LINKS']['LINK_SEPARATOR'])
    links = clean_url(links)
    print(links)
    run_app(links)
