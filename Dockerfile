# Dockerfile-flask
# We simply inherit the Python 3 image. This image does
# not particularly care what OS runs underneath
FROM python:3-onbuild

COPY requirements.txt /usr/src/app
# Install Python dependencies
RUN pip install -r requirements.txt
RUN pip install -U flask-cors
RUN pip install --upgrade pip
# Coping source in current directory into the image
# python:3-onbuild expects the source in /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app

# Commands in a list
#CMD ["python", "source/api/app_rss.py"]
